resource "digitalocean_ssh_key" "default" {
  name       = "Default"
  public_key = file("../support/id_ed25519.pub")
}

