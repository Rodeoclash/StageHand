import * as React from 'react'
import { curry } from 'lodash/fp'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

const handleClickSelect = curry((port, _event) => {
  store.selectedPort = port
})

const handleClickPopout = curry((port, _event) => {
  window.open(port.url, '_blank')
})

export const PreviewSelectOption = ({ port }) => {
  return useObserver(() => {
    const classes = classNames({
      [styles.root]: true,
      [styles.active]: port.url === store.selectedPort.url
    })

    const renderedDescription = (() => {
      if (!port.config.description) {
        return null
      } else {
        return (
          <span className={styles.description}>{port.config.description}</span>
        )
      }
    })()

    return (
      <li className={classes}>
        <span className={styles.about} onClick={handleClickSelect(port)}>
          <a>{port.config.name}</a>
          {renderedDescription}
        </span>
        <span className={styles.popout} onClick={handleClickPopout(port)}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 20 20"
          >
            <path d="M17 17H3V3h5V1H3a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-5h-2z" />
            <path d="M19 1h-8l3.29 3.29-5.73 5.73 1.42 1.42 5.73-5.73L19 9V1z" />
          </svg>
        </span>
      </li>
    )
  })
}

export default PreviewSelectOption
