import React, { useRef, useEffect, useLayoutEffect } from 'react'
import Ansi from 'ansi-to-react'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import styles from './styles.css'

const PreviewLogs = () => {
  const refScroll = useRef(null)

  useEffect(() => {
    if (refScroll.current !== null) {
      const el = refScroll.current
      const isScrolledToBottom =
        el.offsetHeight + el.scrollTop >= el.scrollHeight - 21

      if (isScrolledToBottom) {
        el.scrollTop = el.scrollHeight - el.clientHeight + 1
      }
    }
  })

  useLayoutEffect(() => {
    const el = refScroll.current
    el.scrollTop = el.scrollHeight
  }, [])

  return useObserver(() => {
    const rendered = (() => {
      if (store.preview.logs === null) {
        return <em>Waiting to recieve logs...</em>
      } else {
        return <Ansi>{store.preview.logs}</Ansi>
      }
    })()

    return (
      <div ref={refScroll} className={styles.root}>
        {rendered}
      </div>
    )
  })
}

export default PreviewLogs
