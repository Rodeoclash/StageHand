import * as React from 'react'
import { useObserver } from 'mobx-react-lite'

import store from '@/entries/preview/store.js'

import PreviewSetupStatus from '@/components/PreviewSetupStatus/PreviewSetupStatus.jsx'

import styles from './styles.css'

export const PreviewSetup = () => {
  return useObserver(() => {
    return (
      <div className={styles.root}>
        <div className={styles.message}>
          <h2>Preparing preview</h2>
          <div className={styles.status}>
            <PreviewSetupStatus
              completed={store.preview.provisioning.docker}
              title="Provisioning Docker VM"
            />
            <PreviewSetupStatus
              completed={store.preview.status.code_fetched}
              title="Fetching code"
            />
            <PreviewSetupStatus
              completed={store.preview.status.code_extracted}
              title="Extracting code"
            />
          </div>
        </div>
      </div>
    )
  })
}

export default PreviewSetup
