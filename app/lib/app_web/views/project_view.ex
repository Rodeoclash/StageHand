defmodule AppWeb.ProjectView do
  use AppWeb, :view

  def curl_payload_data(project) do
    %{
      "access_token" => project.access_token,
      "env" => [
        %{
          "name" => "APP_ENV",
          "value" => "production"
        }
      ],
      "repo_slug" => "stagehandtest",
      "sha" => "60256d788f2dbbc9b847553e296f0c746c95b674",
      "username" => "Rodeoclash"
    }
    |> Poison.encode!()
  end
end
