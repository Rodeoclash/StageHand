defmodule AppWeb.Router do
  use AppWeb, :router
  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug(Ueberauth)
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", AppWeb do
    pipe_through(:api)
    post("/preview", Api.PreviewController, :create)
  end

  scope "/auth", AppWeb do
    pipe_through(:browser)

    get("/:provider", AuthorisationController, :request)
    get("/:provider/callback", AuthorisationController, :callback)
  end

  scope "/", AppWeb do
    pipe_through(:browser)
    pipe_through(AppWeb.CurrentUserPipeline)

    scope "/current_user" do
      get("/", CurrentUserController, :show)
    end

    scope "/projects" do
      resources("/", ProjectController, only: [:show, :create]) do
        put("/provider_connection", ProjectController, :create_provider_connection)
      end
    end
  end

  scope "/", AppWeb do
    pipe_through(:browser)
    get("/", ApplicationController, :index)
    get("/preview", PreviewController, :show)
    get("/healthcheck", ApplicationController, :healthcheck)
  end
end
