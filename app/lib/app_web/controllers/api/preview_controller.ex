defmodule AppWeb.Api.PreviewController do
  alias App.{
    Projects,
    Previews,
    Providers
  }

  use AppWeb, :controller

  def create(conn, params) do
    project = Projects.get_by(%{access_token: params["access_token"]})

    provider_name =
      if params["provider"] do
        String.to_atom(params["provider"])
      else
        :bitbucket
      end

    provider = Providers.get(provider_name)

    # Set defaults for missing params
    params =
      Map.merge(
        Previews.Config.default(),
        params
      )

    {:ok, preview} =
      Previews.create(%{
        configuration: params,
        project_id: project.id,
        provider_id: provider.id,
        repo_slug: params["repo_slug"],
        sha: params["sha"],
        username: params["username"],
        version: params["version"]
      })

    Previews.Commits.status(preview, :ready, preview.configuration["feedback"])

    conn
    |> put_status(:created)
    |> render("show.json", preview: preview)
  end
end
