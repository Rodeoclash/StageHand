defmodule App.ProviderAuthorisations.Github do
  @host "https://api.github.com"
  @http_client Application.get_env(:stagehand, :http_client)

  def request(provider_authorisation, method, path, body \\ "", headers \\ []) do
    @http_client.request(
      method,
      Path.join(@host, path),
      Poison.encode!(body),
      Keyword.merge(default_headers(provider_authorisation), headers)
    )
  end

  def default_headers(%{token: token}) do
    [
      Accept: "application/vnd.github.v3+json",
      Authorization: "token #{token}",
      "Content-Type": "application/json"
    ]
  end
end
