defmodule App.ProviderAuthorisations.ProviderAuthorisation do
  alias App.{Users, Providers}
  import Ecto.Changeset
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "provider_authorisations" do
    belongs_to(:provider, Providers.Provider)
    belongs_to(:user, Users.User)
    field(:expires, :boolean)
    field(:expires_at, :integer)
    field(:refresh_token, :string)
    field(:token, :string)
    timestamps()
  end

  @doc false
  def changeset(model, attrs) do
    model
    |> cast(attrs, [
      :expires,
      :expires_at,
      :provider_id,
      :refresh_token,
      :token,
      :user_id
    ])
    |> validate_required([
      :expires,
      :provider_id,
      :token,
      :user_id
    ])
  end
end
