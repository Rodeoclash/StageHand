defmodule App.Roles do
  alias App.{Repo, Roles, Projects}

  @model Roles.Role
  @struct %Roles.Role{}

  def create(attrs) do
    @struct
    |> @model.changeset(attrs)
    |> Repo.insert()
  end

  def get(id) do
    Repo.get(@model, id)
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def name(%Roles.Role{} = role) do
    organsiation = Projects.get(role.project_id)
    "admin@#{organsiation.name || "UnknownOrg"}"
  end
end
