defmodule App.Previews.Commits do
  alias App.{
    Previews,
    Providers
  }

  @doc """
  Set status on the commit to `state`.
  """
  def status(%{provider_id: provider_id} = preview, state, feedback) do
    case Providers.get(provider_id) do
      %{name: name} when name == "bitbucket" ->
        Previews.Bitbucket.Statuses.create(preview, state, feedback)

      %{name: name} when name == "github" ->
        Previews.Github.Statuses.create(preview, state, feedback)
    end
  end
end
