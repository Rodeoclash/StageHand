defmodule App.Previews.KubernetesV2 do
  alias App.{
    KubernetesV2,
    Previews
  }

  require Logger

  @http_client Application.get_env(:stagehand, :http_client)
  @kubernetes_v2 Application.get_env(:stagehand, :kubernetes_v2)
  @utils Application.get_env(:stagehand, :utils)

  @doc """
  Starts the preview in Kubernetes.
  """
  def start(preview) do
    templates = running_templates(preview)

    results = read(preview)

    results
    |> Enum.with_index()
    |> Enum.map(fn {result, idx} ->
      {module, args} = Enum.at(templates, idx)

      case KubernetesV2.result_exists?(result) do
        true -> KubernetesV2.patch(module, args)
        false -> KubernetesV2.create(module, args)
      end
    end)
    |> @kubernetes_v2.async
  end

  @doc """
  Stops the preview in Kubernetes.
  """
  def stop(preview) do
    KubernetesV2.delete(Previews.KubernetesV2.Templates.Namespace, {preview})
    |> @kubernetes_v2.run
  end

  @doc """
  Read all the previews
  """
  def read(preview) do
    running_templates(preview)
    |> Enum.map(fn {module, args} ->
      module.get(args)
    end)
    |> @kubernetes_v2.async
  end

  @doc """
  Gets the preview pod resource, used in catchups and checking if we should launch logs
  """
  @spec fetch_preview_pod(%Previews.Preview{}) :: map()
  def fetch_preview_pod(preview) do
    {:ok, results} =
      Previews.KubernetesV2.Templates.Pod.all(preview)
      |> @kubernetes_v2.run

    results
    |> Map.get("items")
    |> Previews.KubernetesV2.Resources.Pod.preview_pod()
  end

  @doc """
  Fetch the current state of the preview without logs
  """
  @spec catchup(%Previews.Preview{}) :: map()
  def catchup(preview) do
    results = Previews.KubernetesV2.Templates.Pod.all(preview) |> @kubernetes_v2.run

    case results do
      {:ok, results} -> {:ok, build_catchup(preview, results)}
      {:error, results} -> {:error, results}
    end
  end

  @doc """
  Fetch the current state of the preview and includes logs if the preview container is ready

  TODO: Needs error handling
  """
  @spec catchup_with_logs(%Previews.Preview{}) :: map()
  def catchup_with_logs(preview) do
    {:ok, results} =
      Previews.KubernetesV2.Templates.Pod.all(preview)
      |> @kubernetes_v2.run

    preview_pod =
      results
      |> Map.get("items")
      |> Previews.KubernetesV2.Resources.Pod.preview_pod()

    preview_ready? = Previews.KubernetesV2.Resources.Pod.ready(preview_pod, "preview")

    built_catchup = build_catchup(preview, results)

    if preview_ready? == true do
      Map.merge(built_catchup, %{
        logs: logs(preview, Previews.KubernetesV2.Resources.Pod.name(preview_pod))
      })
    else
      built_catchup
    end
  end

  def logs(preview, pod_name) do
    {:ok, result} =
      Previews.KubernetesV2.Templates.Pod.logs({preview}, pod_name)
      |> @kubernetes_v2.run(
        params: %{
          "container" => Previews.KubernetesV2.Templates.Deployment.name()
        }
      )

    result
  end

  @doc """
  Builds an empty catchup structure, used for boot
  """
  @spec catchup_boot(%Previews.Preview{}) :: map()
  def catchup_boot(preview) do
    %{
      provisioning: %{
        docker: false
      },
      readiness:
        Enum.map(preview.configuration["ports"], fn port ->
          %{
            port: port,
            status: :not_ready
          }
        end),
      status: %{
        code_extracted: false,
        code_fetched: false
      }
    }
  end

  @doc """
  Is this preview unhealthy?
  """
  @spec unhealthy?(%Previews.Preview{}) :: boolean()
  def unhealthy?(preview) do
    cond do
      exists?(preview) != true ->
        Logger.info("Unhealthy, full set of resources not found")
        true

      exceeded_restarts?(preview) ->
        Logger.info("Unhealthy, exceeded allowed restarts")
        true

      exceeded_total_runtime?(preview) ->
        Logger.info("Unhealthy, exceeded total allowed runtime")
        true

      expired_updated_at?(preview) ->
        Logger.info("Unhealthy, expired updated at")
        true

      true ->
        false
    end
  end

  @doc """
  Does the complete preview exist?
  """
  def exists?(preview) do
    results = read(preview)
    Enum.all?(results, &KubernetesV2.result_exists?/1)
  end

  @doc """
  Has the preview expired? This is true when we haven't heard the heartbeat for 5 minutes
  """
  def expired_updated_at?(preview) do
    pid = Previews.DynamicSupervisor.Monitor.State.whereis(preview)
    last_viewed_at = GenServer.call(pid, :last_viewed_at)

    # thirty minutes
    DateTime.diff(@utils.utc_now(), last_viewed_at) >=
      Previews.Config.inactivity_timeout_seconds(preview)
  end

  @doc """
  Has this preview expired the total allowed running time?
  """
  @spec exceeded_total_runtime?(%Previews.Preview{}) :: boolean()
  def exceeded_total_runtime?(preview) do
    {:ok, namespace} =
      Previews.KubernetesV2.Templates.Namespace.get({preview})
      |> @kubernetes_v2.run

    {:ok, created_at, _} = Previews.KubernetesV2.Resources.Namespace.created_at(namespace)

    # 2 hours
    DateTime.diff(@utils.utc_now(), created_at) >= 7200
  end

  @doc """
  Has the preview exceeded its allowed restart count?
  """
  @spec exceeded_restarts?(%Previews.Preview{}) :: boolean()
  def exceeded_restarts?(preview) do
    {:ok, pods} =
      Previews.KubernetesV2.Templates.Pod.all(preview, "preview")
      |> @kubernetes_v2.run

    pod =
      pods
      |> Map.get("items")
      |> List.first()

    Previews.KubernetesV2.Resources.Pod.restart_count(pod) >= 5
  end

  @doc """
  Perform a readiness scan against ports and return the results
  """
  @spec readiness(%Previews.Preview{}) :: map()
  def readiness(preview) do
    Enum.map(preview.configuration["ports"], fn port ->
      # Task.async(fn ->
      url =
        Previews.KubernetesV2.Templates.port_url(
          preview,
          port,
          Application.get_env(:stagehand, :preview_domain)
        )

      response =
        @http_client.request(
          :get,
          url
        )

      case response do
        {:error, _error} ->
          %{
            port: port,
            status: :not_ready
          }

        {:ok, %{status_code: status_code}} when status_code == 503 when status_code == 404 ->
          %{
            port: port,
            status: :not_ready
          }

        {:ok, _response} ->
          %{
            port: port,
            status: :ready
          }
      end

      # end)
    end)

    # |> Enum.map(&Task.await/1)
  end

  @doc """
  Given a domain, determines the subdomain for the preview and returns a FQDN to view it. Given to the frontend.
  TODO: Move to KubernetesV2 module
  """
  def preview_url(name, root_domain) do
    %{host: host} = uri = URI.parse(root_domain)

    Map.replace!(uri, :host, name <> "." <> host)
    |> URI.to_string()
  end

  # The following templates with config are required for the app to be running
  defp running_templates(preview) do
    namespaces = [
      {Previews.KubernetesV2.Templates.Namespace, {preview}}
    ]

    services =
      preview.configuration["ports"]
      |> Enum.map(fn port ->
        {Previews.KubernetesV2.Templates.Service, {preview, port}}
      end)

    services = services ++ [{Previews.KubernetesV2.Templates.ServiceDocker, {preview}}]

    deployments = [
      {Previews.KubernetesV2.Templates.Deployment, {preview}},
      {Previews.KubernetesV2.Templates.DeploymentDocker, {preview}}
    ]

    namespaces ++ services ++ deployments
  end

  defp build_catchup(preview, results) do
    state =
      results
      |> Map.get("items")
      |> Previews.KubernetesV2.Resources.Pod.state()

    Map.merge(state, %{
      readiness: readiness(preview)
    })
  end
end
