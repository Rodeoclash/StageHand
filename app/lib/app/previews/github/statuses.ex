defmodule App.Previews.Github.Statuses do
  alias App.{
    Previews,
    ProviderAuthorisations
  }

  def create(preview, state, feedback) do
    provider_authorisation = Previews.ProviderAuthorisations.get(preview)

    state =
      case state do
        :ready -> "success"
      end

    ProviderAuthorisations.Github.Statuses.create(
      provider_authorisation,
      preview.username,
      preview.repo_slug,
      preview.sha,
      %{
        description: feedback["name"],
        context: feedback["key"],
        state: state,
        target_url:
          AppWeb.Router.Helpers.preview_url(AppWeb.Endpoint, :show, %{
            id: preview.id
          })
      }
    )
  end
end
