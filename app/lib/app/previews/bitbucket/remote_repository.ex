defmodule App.Previews.Bitbucket.RemoteRepository do
  alias App.{
    Previews,
    ProviderAuthorisations
  }

  def download_url(bitbucket_preview) do
    "https://bitbucket.org/#{bitbucket_preview.username}/#{bitbucket_preview.repo_slug}/get/#{
      bitbucket_preview.sha
    }.tar.gz"
  end

  def auth_header(bitbucket_preview) do
    provider_authorisation = Previews.ProviderAuthorisations.get(bitbucket_preview)

    {:ok, provider_authorisation} =
      ProviderAuthorisations.Bitbucket.Token.renew_if_expired(provider_authorisation)

    "Authorization: Bearer #{provider_authorisation.token}"
  end
end
