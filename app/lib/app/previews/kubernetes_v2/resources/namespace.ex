defmodule App.Previews.KubernetesV2.Resources.Namespace do
  alias App.{
    KubernetesV2
  }

  @doc """
  Returns the time of creation for this preview
  """
  @spec created_at(map) :: map()
  def created_at(namespace) do
    namespace
    |> Map.get("metadata")
    |> Map.get("creationTimestamp")
    |> DateTime.from_iso8601()
  end

  @doc """
  Returns the updated_at component of a resource result
  """
  @spec updated_at(map) :: map()
  def updated_at(namespace) do
    namespace
    |> KubernetesV2.Templates.Annotation.parse()
    |> Map.get("updated_at")
    |> DateTime.from_iso8601()
  end
end
