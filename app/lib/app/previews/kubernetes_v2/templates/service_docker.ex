defmodule App.Previews.KubernetesV2.Templates.ServiceDocker do
  @moduledoc """
  Exposes the Docker daemon internally
  """

  alias App.{
    Previews
  }

  @doc """
  Name for this resource
  """
  @spec name() :: String.t()
  def name() do
    "docker"
  end

  @doc """
  Operation to determine if the resource exists
  """
  @spec get({%Previews.Preview{}}) :: K8s.Operation.t()
  def get({preview}) do
    K8s.Client.get("v1", :service,
      namespace: Previews.KubernetesV2.Templates.namespace(preview),
      name: name()
    )
  end

  @doc """
  Define the template for the resource
  """
  @spec template({%Previews.Preview{}}) :: map()
  def template({preview}) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "name" => name(),
        "namespace" => Previews.KubernetesV2.Templates.namespace(preview)
      },
      "spec" => %{
        "selector" => %{
          "app" => Previews.KubernetesV2.Templates.DeploymentDocker.name()
        },
        "ports" => [
          %{
            "port" => 2375,
            "targetPort" => 2375,
            "protocol" => "TCP"
          }
        ]
      }
    }
  end
end
