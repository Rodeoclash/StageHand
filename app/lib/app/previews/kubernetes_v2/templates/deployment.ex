defmodule App.Previews.KubernetesV2.Templates.Deployment do
  @moduledoc """
  Defines a service that exposes a preview (or side container) running in the preview
  """

  alias App.{
    Previews,
    Providers
  }

  @doc """
  Name of this deployment
  """
  @spec name() :: String.t()
  def name, do: "preview"

  @doc """
  Operation to determine if the resource exists
  """
  @spec get({%Previews.Preview{}}) :: K8s.Operation.t()
  def get({preview}) do
    K8s.Client.get("apps/v1", :deployment,
      namespace: Previews.KubernetesV2.Templates.namespace(preview),
      name: name()
    )
  end

  @doc """
  Creates data based on the provider config for use in the deployment.
  """
  @spec provider_config({%Previews.Preview{}}) :: map()
  def provider_config({preview}) do
    case Providers.get(preview.provider_id) do
      %{name: name} when name == "bitbucket" ->
        %{
          auth_header: Previews.Bitbucket.RemoteRepository.auth_header(preview),
          download_url: Previews.Bitbucket.RemoteRepository.download_url(preview)
        }

      %{name: name} when name == "github" ->
        %{
          auth_header: Previews.Github.RemoteRepository.auth_header(preview),
          download_url: Previews.Github.RemoteRepository.download_url(preview)
        }
    end
  end

  @doc """
  Creates environment variables for the preview.

  Default environment variables like the public host of the preview should also be set here.
  """
  @spec env({%Previews.Preview{}}) :: nonempty_list(map())
  def env({%{configuration: %{"env" => env, "ports" => ports}} = preview}) do
    port_hosts =
      Enum.map(ports, fn port ->
        %{
          "name" => "PORT_#{port["value"]}_HOST",
          "value" =>
            Previews.KubernetesV2.Templates.port_url(
              preview,
              port,
              Application.get_env(:stagehand, :preview_domain)
            )
        }
      end)

    [
      %{
        "name" => "DOCKER_HOST",
        "value" => "tcp://#{Previews.KubernetesV2.Templates.ServiceDocker.name()}:2375"
      }
    ] ++ port_hosts ++ env
  end

  @doc """
  Define the template for the resource
  """
  @spec template({%Previews.Preview{}}) :: map()
  def template({preview} = args) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{
        "name" => name(),
        "namespace" => Previews.KubernetesV2.Templates.namespace(preview)
      },
      "spec" => %{
        "replicas" => 1,
        "selector" => %{
          "matchLabels" => %{
            "app" => name()
          }
        },
        "template" => %{
          "metadata" => %{
            "labels" => %{
              "app" => name()
            }
          },
          "spec" => %{
            "containers" => [
              %{
                "name" => "preview",
                "image" => "tmaier/docker-compose:19",
                "command" => [
                  "sh",
                  "-c",
                  "cd /repo-contents && #{preview.configuration["command"]}"
                ],
                "env" => env(args),
                "volumeMounts" => [
                  %{
                    "name" => "repo-contents",
                    "mountPath" => "/repo-contents"
                  }
                ]
              }
            ],
            "initContainers" => [
              %{
                "name" => "fetch-code",
                "image" => "appropriate/curl:latest",
                "command" => [
                  "curl",
                  "--location",
                  "--header",
                  provider_config(args)[:auth_header],
                  "--output",
                  "/repo-download/archive.tar.gz",
                  provider_config(args)[:download_url]
                ],
                "volumeMounts" => [
                  %{
                    "name" => "repo-download",
                    "mountPath" => "/repo-download"
                  }
                ]
              },
              %{
                "name" => "extract-code",
                "image" => "busybox:latest",
                "command" => [
                  "tar",
                  "--strip-components=1",
                  "--extract",
                  "--verbose",
                  "--file=/repo-download/archive.tar.gz",
                  "--directory",
                  "/repo-contents"
                ],
                "volumeMounts" => [
                  %{
                    "name" => "repo-download",
                    "mountPath" => "/repo-download"
                  },
                  %{
                    "name" => "repo-contents",
                    "mountPath" => "/repo-contents"
                  }
                ]
              }
            ],
            "volumes" => [
              %{
                "name" => "repo-download",
                "emptyDir" => %{}
              },
              %{
                "name" => "repo-contents",
                "emptyDir" => %{}
              }
            ]
          }
        }
      }
    }
  end
end
