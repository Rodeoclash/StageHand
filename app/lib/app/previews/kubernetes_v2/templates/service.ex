defmodule App.Previews.KubernetesV2.Templates.Service do
  @moduledoc """
  Defines a service that exposes a preview (or side container) running in the preview
  """

  alias App.{
    KubernetesV2,
    Previews
  }

  @doc """
  Name for the service, also doubles as the hostname
  """
  @spec name({%Previews.Preview{}, map(), integer()}) :: K8s.Operation.t()
  def name({preview, port}) do
    "#{KubernetesV2.kid(preview.id)}-#{Previews.KubernetesV2.Templates.port_name(preview, port)}"
  end

  @doc """
  Operation to determine if the resource exists
  """
  @spec get({%Previews.Preview{}, map(), integer()}) :: K8s.Operation.t()
  def get({preview, _port} = args) do
    K8s.Client.get("v1", :service,
      namespace: Previews.KubernetesV2.Templates.namespace(preview),
      name: name(args)
    )
  end

  def port_host_regex(preview, port) do
    ".*#{Previews.KubernetesV2.Templates.port_name(preview, port)}.*"
  end

  @doc """
  Define the template for the resource
  """
  @spec template({%Previews.Preview{}, map()}) :: map()
  def template({preview, port} = args) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "name" => "#{name(args)}",
        "namespace" => Previews.KubernetesV2.Templates.namespace(preview),
        "annotations" => %{
          "getambassador.io/config" => """
          ---
          apiVersion: "ambassador/v1"
          host: "#{port_host_regex(preview, port)}"
          host_regex: true
          kind: "Mapping"
          name: "#{name(args)}"
          prefix: "/"
          service: "#{name(args)}.#{Previews.KubernetesV2.Templates.namespace(preview)}"
          timeout_ms: 60000
          connect_timeout_ms: 60000
          use_websocket: true
          remove_response_headers:
            - x-frame-options
          """
        }
      },
      "spec" => %{
        "selector" => %{
          "app" => Previews.KubernetesV2.Templates.DeploymentDocker.name()
        },
        "ports" => [
          %{
            "name" => Previews.KubernetesV2.Templates.port_name(preview, port),
            "port" => 80,
            "targetPort" => Previews.KubernetesV2.Templates.port_name(preview, port)
          }
        ]
      }
    }
  end
end
