defmodule App.Previews do
  alias App.{
    KubernetesV2,
    Previews,
    Repo
  }

  require Logger

  @model Previews.Preview
  @struct %Previews.Preview{}

  def create(attrs) do
    @struct
    |> @model.changeset(attrs)
    |> Repo.insert()
  end

  def update(model, attrs) do
    model
    |> @model.changeset(attrs)
    |> Repo.update()
  end

  def get(id) do
    Repo.get(@model, id)
  end

  def get_by(queryable) do
    Repo.get_by(@model, queryable)
  end

  def get_or_create_by(attrs) do
    case get_by(attrs) do
      nil ->
        create(attrs)

      model ->
        {:ok, model}
    end
  end

  @doc """
  Get an id from the preview useful for use in K8s
  """
  @spec kid(%Previews.Preview{}) :: String.t()
  def kid(preview) do
    KubernetesV2.kid(preview.id)
  end

  @doc """
  Launches the preview by creating it in Kubernetes and setting up monitoring.
  # TODO: Test monitoring ran
  """
  def launch(preview) do
    Logger.info("Launching preview #{preview.id}")

    with(
      nil <- Previews.Launch.kubernetes(preview),
      {:ok, _supervisor_pid} <- Previews.DynamicSupervisor.start_child(preview),
      {:ok, _pid_health, _pid_state, _pid_status} <-
        Previews.DynamicSupervisor.Monitor.start_monitoring(preview)
    ) do
      {:ok, :success}
    else
      {:error, error} ->
        case error do
          %HTTPoison.Response{status_code: 403} ->
            {:error, :forbidden}

          # This can occur as a race condition between the preview starting in K8s and the monitor picking up an incomplete preview.
          {:already_started, _pid} ->
            {:error, :already_started}

          _ ->
            Sentry.capture_message("unknown_error_failed_preview_launch",
              extra: %{preview: inspect(preview), message: inspect(error)}
            )

            {:error, :unknown}
        end
    end
  end

  @doc """
  Terminates the preview by removing it from Kubernetes and stopping monitoring of it

  If this process self terminates, e.g. failing health check, it won't progress past the terminate_child method.
  """
  def terminate(preview) do
    Logger.info("Terminating preview #{preview.id}")

    with(
      {:ok, _response} <- Previews.KubernetesV2.stop(preview),
      :ok <- Previews.DynamicSupervisor.terminate_child(preview)
    ) do
      :ok
    else
      {:error, message} -> {:error, message}
    end
  end

  def log(preview, :info, prefix, message) do
    Logger.info("#{kid(preview)}: [#{prefix}]: #{message}")
  end
end
