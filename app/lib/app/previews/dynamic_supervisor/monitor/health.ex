defmodule App.Previews.DynamicSupervisor.Monitor.Health do
  alias App.{
    Previews
  }

  use GenServer
  use Previews.DynamicSupervisor.Monitor.Base, name: :monitor_health

  # seconds
  @tick_rate 5

  def init([preview]) do
    state = %{
      preview: preview
    }

    schedule_work()

    {:ok, state}
  end

  @doc """
  Executed periodically, this function determines if the preview is in a healthy state. If not, it's terminated
  """
  def handle_info(:work, %{preview: preview} = state) do
    log(preview, "Running check now")

    if Previews.KubernetesV2.unhealthy?(preview) == true do
      log(preview, "Preview found in an unhealthy state, terminating")
      Previews.terminate(preview)
    else
      schedule_work()
    end

    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, tick_length())
  end

  defp tick_length do
    @tick_rate * 1000
  end
end
