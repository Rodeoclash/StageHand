defmodule App.Previews.DynamicSupervisor.Monitor.State do
  @moduledoc """
  Tracks state about a preview (currently, just the last_viewed_at)
  """

  alias App.{
    Previews
  }

  use GenServer
  use Previews.DynamicSupervisor.Monitor.Base, name: :monitor_state

  @utils Application.get_env(:stagehand, :utils)

  @impl true
  def init([_preview]) do
    state = %{
      last_viewed_at: @utils.utc_now()
    }

    {:ok, state}
  end

  @impl true
  def handle_call(:last_viewed_at, _from, %{last_viewed_at: last_viewed_at} = state) do
    {:reply, last_viewed_at, state}
  end

  @impl true
  def handle_cast({:last_viewed_at, last_viewed_at}, _state) do
    state = %{
      last_viewed_at: last_viewed_at
    }

    {:noreply, state}
  end
end
