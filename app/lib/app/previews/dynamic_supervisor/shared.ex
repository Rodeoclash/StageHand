defmodule App.Previews.DynamicSupervisor.Shared do
  @doc """
  Is the supervisor for this preview running?
  """
  def exists?(name) do
    whereis(name)
    |> is_pid
  end

  @doc """
  Returns pid for this server or nil if not found
  """
  def whereis(name) do
    case Registry.lookup(:previews, name) do
      [] -> nil
      [{pid, _value}] -> pid
    end
  end
end
