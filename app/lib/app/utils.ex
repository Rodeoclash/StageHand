defmodule App.Utils do
  @moduledoc """
  Provides useful functions that don't really belong anywhere else.
  """

  @callback utc_now() :: String.t()

  @spec utc_now() :: DateTime.t()
  def utc_now do
    DateTime.utc_now()
  end
end
