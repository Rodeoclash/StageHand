defmodule App.Users.Authorisation do
  alias App.{
    ProviderAuthorisations,
    Providers
  }

  def find_or_create(
        %Ueberauth.Auth{
          provider: provider_name,
          credentials: %Ueberauth.Auth.Credentials{
            expires: expires,
            expires_at: expires_at,
            refresh_token: refresh_token,
            token: token
          }
        },
        user
      ) do
    provider = Providers.get(provider_name)

    case ProviderAuthorisations.get_by(%{
           provider_id: provider.id,
           user_id: user.id
         }) do
      nil ->
        ProviderAuthorisations.create(%{
          expires: expires,
          expires_at: expires_at,
          provider_id: provider.id,
          refresh_token: refresh_token,
          token: token,
          user_id: user.id
        })

      provider_authorisation ->
        ProviderAuthorisations.update(provider_authorisation, %{
          expires: expires,
          expires_at: expires_at,
          refresh_token: refresh_token,
          token: token
        })
    end
  end
end
