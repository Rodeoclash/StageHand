defmodule App.KubernetesV2 do
  @moduledoc """
  Wrap the intersection between Stagehand and Kubernetes
  """

  alias App.{
    Projects,
    Previews
  }

  @callback async(list(K8s.Operation.t())) :: list(K8s.Client.Runner.Base.result_t())
  @callback run(K8s.Operation.t(), keyword(atom)) :: K8s.Client.Runner.Base.result_t()
  @callback run(K8s.Operation.t()) :: K8s.Client.Runner.Base.result_t()
  @callback watch(K8s.Operation.t(), keyword(atom)) :: K8s.Client.Runner.Base.result_t()
  @callback watch(K8s.Operation.t(), keyword(atom), keyword(atom)) ::
              K8s.Client.Runner.Base.result_t()

  @type template_modules :: Projects.KubernetesV2.Templates.Namespace
  @type template_models :: %Projects.Project{} | %Previews.Preview{}

  @default_opts [recv_timeout: 10_000]

  @spec async(list(K8s.Operation.t())) :: list(K8s.Client.Runner.Base.result_t())
  def async(operations) do
    K8s.Client.async(operations, :default, @default_opts)
  end

  @spec run(K8s.Operation.t(), keyword(atom)) :: K8s.Client.Runner.Base.result_t()
  def run(operations, opts) do
    K8s.Client.run(operations, :default, Keyword.merge(@default_opts, opts))
  end

  @spec run(K8s.Operation.t()) :: K8s.Client.Runner.Base.result_t()
  def run(operations) do
    K8s.Client.run(operations, :default, @default_opts)
  end

  @spec watch(K8s.Operation.t(), keyword(atom)) :: K8s.Client.Runner.Base.result_t()
  def watch(operation, opts) do
    K8s.Client.watch(operation, :default, opts)
  end

  @spec watch(K8s.Operation.t(), keyword(atom), keyword(atom)) ::
          K8s.Client.Runner.Base.result_t()
  def watch(operation, opts, opts2) do
    K8s.Client.watch(operation, :default, opts, opts2)
  end

  @doc """
  Given an id string, returns a K8s suitable id
  """
  @spec kid(String.t()) :: String.t()
  def kid(id) do
    result =
      :crypto.hash(:sha256, id)
      |> Base.encode16(case: :lower)
      |> String.slice(0..12)

    "id" <> result
  end

  @doc """
  Delete the provided template
  """
  @spec delete(template_modules, template_models) :: K8s.Operation.t()
  def delete(module, args) do
    template = module.template(args)
    K8s.Client.delete(template)
  end

  @doc """
  Create the provided template
  """
  @spec create(template_modules, template_models) :: K8s.Operation.t()
  def create(module, args) do
    template = module.template(args)
    K8s.Client.create(template)
  end

  @doc """
  Patch the provided template
  """
  @spec patch(template_modules, template_models) :: K8s.Operation.t()
  def patch(module, args) do
    template = module.template(args)
    K8s.Client.patch(template)
  end

  def result_exists?(result) do
    case result do
      {:ok, _result} -> true
      {:error, :not_found} -> false
    end
  end
end
