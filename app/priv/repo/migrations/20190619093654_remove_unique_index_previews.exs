defmodule App.Repo.Migrations.RemoveUniqueIndexPreviews do
  use Ecto.Migration

  def change do
    drop unique_index(:previews, [
      :organisation_id,
      :provider_id,
      :repo_slug,
      :sha, :username
    ])
  end
end
