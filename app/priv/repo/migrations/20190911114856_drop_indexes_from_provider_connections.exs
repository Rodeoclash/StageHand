defmodule App.Repo.Migrations.DropIndexesFromProviderConnections do
  use Ecto.Migration

  def change do
    drop unique_index(:provider_connections, [:organisation_id])
    drop unique_index(:provider_connections, [:organisation_id, :user_id])
    drop unique_index(:provider_connections, [:organisation_id, :provider_id])
    create unique_index(:provider_connections, [:organisation_id, :provider_id, :user_id])
  end
end
