defmodule App.RolesTest do
  alias App.Roles
  import App.Fixtures
  use App.DataCase, async: true

  test "create/1" do
    user = insert(:user)
    project = insert(:project)

    attrs = %{
      project_id: project.id,
      user_id: user.id
    }

    assert {:ok, role} = Roles.create(attrs)
    assert %Roles.Role{} = role
    assert role.user_id == user.id
    assert role.project_id == project.id
  end

  test "get/1" do
    role = insert(:role)
    assert Roles.get(role.id).id == role.id
  end

  test "get_by/1" do
    inserted_role = insert(:role)

    assert role =
             Roles.get_by(
               project_id: inserted_role.project_id,
               user_id: inserted_role.user_id
             )

    assert inserted_role.id == role.id
  end

  test "name/1" do
    name = "Fun World"
    project = insert(:project, %{name: name})
    role = insert(:role, %{project: project})
    assert Roles.name(role) == "admin@#{name}"
  end
end
