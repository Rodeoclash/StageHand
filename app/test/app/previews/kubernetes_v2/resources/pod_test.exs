defmodule App.Previews.KubernetesV2.Resources.PodTest do
  alias App.{
    Previews.KubernetesV2.Resources.Pod
  }

  use App.DataCase, async: true

  @docker_container_status %{
    "name" => "docker",
    "restartCount" => 3,
    "ready" => false
  }

  @preview_container_status %{
    "name" => "preview",
    "restartCount" => 4,
    "ready" => true
  }

  @init_container_fetch %{
    "name" => "fetch-code",
    "state" => %{
      "terminated" => %{
        "reason" => "Completed"
      }
    }
  }

  @init_container_extract %{
    "name" => "extract-code",
    "state" => %{
      "waiting" => %{
        "reason" => "CrashLoopBackOff"
      }
    }
  }

  @docker_pod_status %{
    "containerStatuses" => [
      @docker_container_status
    ],
    "phase" => "Pending"
  }

  @preview_pod_status %{
    "containerStatuses" => [
      @preview_container_status
    ],
    "initContainerStatuses" => [
      @init_container_fetch,
      @init_container_extract
    ],
    "phase" => "Running"
  }

  setup do
    docker_pod = %{
      "metadata" => %{
        "name" => "name",
        "labels" => %{
          "app" => "docker"
        }
      },
      "status" => @docker_pod_status
    }

    preview_pod = %{
      "metadata" => %{
        "name" => "name",
        "labels" => %{
          "app" => "preview"
        }
      },
      "status" => @preview_pod_status
    }

    %{
      docker_pod: docker_pod,
      preview_pod: preview_pod
    }
  end

  test "state/1", %{docker_pod: docker_pod, preview_pod: preview_pod} do
    assert Pod.state([docker_pod, preview_pod]) == %{
             provisioning: %{docker: false},
             status: %{code_extracted: false, code_fetched: true}
           }
  end

  test "docker_pod/1", %{docker_pod: docker_pod, preview_pod: preview_pod} do
    assert Pod.docker_pod([docker_pod, preview_pod]) == docker_pod
  end

  test "preview_pod/1", %{docker_pod: docker_pod, preview_pod: preview_pod} do
    assert Pod.preview_pod([docker_pod, preview_pod]) == preview_pod
  end

  test "name/1", %{docker_pod: docker_pod} do
    assert Pod.name(docker_pod) == "name"
  end

  test "status/1", %{docker_pod: docker_pod} do
    assert Pod.status(docker_pod) == @docker_pod_status
  end

  test "has_status/1", %{docker_pod: docker_pod} do
    assert Pod.has_status(docker_pod) == true
  end

  describe "phase/1" do
    test "preview", %{preview_pod: preview_pod} do
      assert Pod.phase(preview_pod) == "Running"
    end

    test "docker", %{docker_pod: docker_pod} do
      assert Pod.phase(docker_pod) == "Pending"
    end
  end

  describe "container_status/2" do
    test "preview", %{preview_pod: preview_pod} do
      assert Pod.container_status(preview_pod, "preview") == @preview_container_status
    end

    test "docker", %{docker_pod: docker_pod} do
      assert Pod.container_status(docker_pod, "docker") == @docker_container_status
    end
  end

  describe "init_container_status/2" do
    test "fetch", %{preview_pod: preview_pod} do
      assert Pod.init_container_status(preview_pod, "fetch-code") == @init_container_fetch
    end

    test "extract", %{preview_pod: preview_pod} do
      assert Pod.init_container_status(preview_pod, "extract-code") == @init_container_extract
    end
  end

  test "restart_count/1", %{preview_pod: preview_pod} do
    assert Pod.restart_count(preview_pod) == 4
  end

  describe "ready/2" do
    test "preview", %{preview_pod: preview_pod} do
      assert Pod.ready(preview_pod, "preview") == true
    end

    test "docker", %{docker_pod: docker_pod} do
      assert Pod.ready(docker_pod, "docker") == false
    end
  end

  test "code_fetched/1", %{preview_pod: preview_pod} do
    assert Pod.code_fetched(preview_pod) == true
  end

  test "code_extracted/1", %{preview_pod: preview_pod} do
    assert Pod.code_extracted(preview_pod) == false
  end
end
