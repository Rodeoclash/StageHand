defmodule App.KubernetesV2.Templates.DeploymentTest do
  alias App.{
    Mocks.KubernetesV2.Operation,
    Previews.KubernetesV2.Templates.Deployment
  }

  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider_bitbucket = insert(:provider, name: "bitbucket")
    provider_github = insert(:provider, name: "github")

    project_bitbucket = insert(:project)
    project_github = insert(:project, id: "83b1101f-45ff-40fe-8618-3abcfd0452d7")

    provider_authorisation_bitbucket =
      insert(:provider_authorisation,
        provider: provider_bitbucket,
        expires_at: 1_654_724_864,
        token: "1"
      )

    provider_authorisation_github =
      insert(:provider_authorisation, provider: provider_github, expires_at: nil, token: "1")

    insert(:provider_connection,
      provider: provider_bitbucket,
      user: provider_authorisation_bitbucket.user,
      project: project_bitbucket
    )

    insert(:provider_connection,
      provider: provider_github,
      user: provider_authorisation_github.user,
      project: project_github
    )

    preview_bitbucket =
      insert(:preview, %{
        provider: provider_bitbucket,
        project: project_bitbucket,
        configuration: %{
          "env" => [
            %{
              "name" => "APP_ENV",
              "value" => "local"
            }
          ],
          "ports" => [
            %{
              "name" => "App",
              "value" => 80
            },
            %{
              "name" => "Storybook",
              "value" => 8081
            }
          ]
        }
      })

    preview_github =
      insert(:preview, %{
        id: "29e41d9a-4ca9-484b-82c1-df01dff3ab41",
        provider: provider_github,
        project: project_github,
        configuration: %{
          "env" => [
            %{
              "name" => "APP_ENV",
              "value" => "local"
            }
          ],
          "ports" => [
            %{
              "name" => "App",
              "value" => 80
            },
            %{
              "name" => "Storybook",
              "value" => 8081
            }
          ]
        }
      })

    %{
      preview_bitbucket: preview_bitbucket,
      preview_github: preview_github
    }
  end

  test "name/0" do
    assert Deployment.name() == "preview"
  end

  test "get/1", %{preview_bitbucket: preview} do
    assert Deployment.get({preview}) ==
             Operation.Deployment.get(preview)
  end

  describe "provider_config/1" do
    test "with bitbucket", %{preview_bitbucket: preview} do
      assert Deployment.provider_config({preview}) == %{
               auth_header: "Authorization: Bearer 1",
               download_url: "https://bitbucket.org/username/repo_slug/get/sha.tar.gz"
             }
    end

    test "with github", %{preview_github: preview} do
      assert Deployment.provider_config({preview}) == %{
               auth_header: "Authorization: token 1",
               download_url: "https://github.com/username/repo_slug/tarball/sha"
             }
    end
  end

  test "env/1", %{preview_bitbucket: preview} do
    assert Deployment.env({preview}) == [
             %{
               "name" => "DOCKER_HOST",
               "value" => "tcp://docker:2375"
             },
             %{
               "name" => "PORT_80_HOST",
               "value" => "http://id39da6a4e7d8ae.stagehand.home:32568"
             },
             %{
               "name" => "PORT_8081_HOST",
               "value" => "http://id0e3ba36e89c70.stagehand.home:32568"
             },
             %{
               "name" => "APP_ENV",
               "value" => "local"
             }
           ]
  end

  test "template/1", %{preview_bitbucket: preview} do
    assert Deployment.template({preview}) == %{
             "apiVersion" => "apps/v1",
             "kind" => "Deployment",
             "metadata" => %{"namespace" => "id9f12b94792201", "name" => "preview"},
             "spec" => %{
               "replicas" => 1,
               "selector" => %{"matchLabels" => %{"app" => "preview"}},
               "template" => %{
                 "metadata" => %{"labels" => %{"app" => "preview"}},
                 "spec" => %{
                   "initContainers" => [
                     %{
                       "command" => [
                         "curl",
                         "--location",
                         "--header",
                         "Authorization: Bearer 1",
                         "--output",
                         "/repo-download/archive.tar.gz",
                         "https://bitbucket.org/username/repo_slug/get/sha.tar.gz"
                       ],
                       "image" => "appropriate/curl:latest",
                       "name" => "fetch-code",
                       "volumeMounts" => [
                         %{"mountPath" => "/repo-download", "name" => "repo-download"}
                       ]
                     },
                     %{
                       "command" => [
                         "tar",
                         "--strip-components=1",
                         "--extract",
                         "--verbose",
                         "--file=/repo-download/archive.tar.gz",
                         "--directory",
                         "/repo-contents"
                       ],
                       "image" => "busybox:latest",
                       "name" => "extract-code",
                       "volumeMounts" => [
                         %{"mountPath" => "/repo-download", "name" => "repo-download"},
                         %{"mountPath" => "/repo-contents", "name" => "repo-contents"}
                       ]
                     }
                   ],
                   "containers" => [
                     %{
                       "command" => ["sh", "-c", "cd /repo-contents && "],
                       "image" => "tmaier/docker-compose:19",
                       "name" => "preview",
                       "volumeMounts" => [
                         %{"mountPath" => "/repo-contents", "name" => "repo-contents"}
                       ],
                       "env" => [
                         %{"name" => "DOCKER_HOST", "value" => "tcp://docker:2375"},
                         %{
                           "name" => "PORT_80_HOST",
                           "value" => "http://id39da6a4e7d8ae.stagehand.home:32568"
                         },
                         %{
                           "name" => "PORT_8081_HOST",
                           "value" => "http://id0e3ba36e89c70.stagehand.home:32568"
                         },
                         %{"name" => "APP_ENV", "value" => "local"}
                       ]
                     }
                   ],
                   "volumes" => [
                     %{"emptyDir" => %{}, "name" => "repo-download"},
                     %{"emptyDir" => %{}, "name" => "repo-contents"}
                   ]
                 }
               }
             }
           }
  end
end
