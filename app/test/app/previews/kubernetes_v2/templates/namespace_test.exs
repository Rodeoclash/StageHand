defmodule App.Previews.KubernetesV2.Templates.NamespaceTest do
  alias App.{
    Mocks.KubernetesV2.Operation,
    Previews.KubernetesV2.Templates.Namespace
  }

  import App.Fixtures
  use App.DataCase

  setup do
    preview = insert(:preview)
    %{preview: preview}
  end

  test "all/1" do
    assert Namespace.all() == Operation.Namespace.list()
  end

  test "get/1", %{preview: preview} do
    assert Namespace.get({preview}) == Operation.Namespace.get(preview)
  end

  test "template/1", %{preview: preview} do
    assert Namespace.template({preview}) == %{
             "apiVersion" => "v1",
             "kind" => "Namespace",
             "metadata" => %{
               "name" => "id9f12b94792201",
               "annotations" => %{
                 "stagehand" => "{}"
               },
               "labels" => %{
                 "preview_id" => "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
                 "stagehand" => "true"
               }
             }
           }
  end
end
