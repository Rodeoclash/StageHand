defmodule App.Previews.ConfigTest do
  alias App.{
    Previews.Config
  }

  import App.Fixtures
  use App.DataCase, async: true

  @configuration %{
    "command" => "./stagehand.sh",
    "env" => [],
    "feedback" => %{
      "description" => "View in Stagehand",
      "key" => "stagehand",
      "name" => "Stagehand Preview"
    },
    "inactivity_timeout_seconds" => 7200,
    "ports" => [
      %{
        "name" => "App",
        "value" => 80
      }
    ],
    "version" => 1
  }

  test "default/0" do
    assert Config.default() == @configuration
  end

  describe "inactivity_timeout_seconds/1" do
    test "when config option exists" do
      preview =
        insert(:preview, %{
          configuration: @configuration
        })

      assert Config.inactivity_timeout_seconds(preview) == 7200
    end

    test "when option does not exist" do
      preview = insert(:preview)

      assert Config.inactivity_timeout_seconds(preview) == 300
    end
  end
end
