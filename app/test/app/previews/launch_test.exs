defmodule App.Previews.LaunchTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews.Launch
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: false

  setup :set_mox_global
  setup :verify_on_exit!

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)
    insert(:provider_authorisation, user: user, provider: provider, expires_at: 1_654_724_864)
    insert(:provider_connection, project: project, provider: provider, user: user)
    preview = insert(:preview, project: project, provider: provider)

    %{preview: preview}
  end

  describe "launch/1" do
    test "with no errors", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :async,
        Expectation.read_all(preview, Response.read_all_missing(preview))
      )

      App.KubernetesV2Mock
      |> expect(:async, Expectation.start(preview, Response.create_all(preview)))

      assert nil == Launch.kubernetes(preview)
    end

    test "with error in creating", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :async,
        Expectation.read_all(preview, Response.read_all_missing(preview))
      )

      App.KubernetesV2Mock
      |> expect(:async, Expectation.start(preview, Response.create_error_forbidden(preview)))

      assert {:error,
              %HTTPoison.Response{
                body: nil,
                headers: [],
                request: nil,
                request_url: nil,
                status_code: 403
              }} == Launch.kubernetes(preview)
    end
  end
end
