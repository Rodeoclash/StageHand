defmodule App.Previews.Github.RemoteRepositoryTest do
  alias App.{Previews.Github.RemoteRepository}
  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider = insert(:provider, name: "github")
    project = insert(:project)
    user = insert(:user)

    provider_authorisation = insert(:provider_authorisation, user: user, provider: provider)

    insert(:provider_connection, project: project, provider: provider, user: user)
    preview = insert(:preview, project: project, provider: provider)

    %{
      preview: preview,
      provider_authorisation: provider_authorisation
    }
  end

  test "download_url/1", %{
    preview: preview
  } do
    assert RemoteRepository.download_url(preview) ==
             "https://github.com/username/repo_slug/tarball/sha"
  end

  test "auth_header/1", %{
    preview: preview,
    provider_authorisation: provider_authorisation
  } do
    assert RemoteRepository.auth_header(preview) ==
             "Authorization: token #{provider_authorisation.token}"
  end
end
