defmodule App.Previews.DynamicSupervisor.Monitor.StateTest do
  alias App.{
    Previews.DynamicSupervisor.Monitor.State
  }

  import App.Fixtures
  import Mox
  use App.DataCase, async: false

  setup :verify_on_exit!
  setup :set_mox_global

  setup do
    preview = insert(:preview)

    %{
      preview: preview
    }
  end

  test "name/1", %{preview: preview} do
    assert State.name(preview) == {:monitor_state, "id9f12b94792201"}
  end

  test "via_name/1", %{preview: preview} do
    assert State.via_name(preview) ==
             {:via, Registry, {:previews, {:monitor_state, "id9f12b94792201"}}}
  end

  test "start_link/1", %{preview: preview} do
    {:ok, datetime, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")

    App.UtilsMock
    |> expect(:utc_now, fn ->
      datetime
    end)

    assert {:ok, pid} = State.start_link([preview])
    assert is_pid(pid)
    GenServer.stop(pid)
    refute Process.alive?(pid)
  end

  test "init/1", %{preview: preview} do
    {:ok, datetime, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")

    App.UtilsMock
    |> expect(:utc_now, fn ->
      datetime
    end)

    assert {:ok, state} = State.init([preview])
    assert state[:last_viewed_at] == datetime
  end

  test "handle_call/3" do
    {:ok, last_viewed_at, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")

    state = %{
      last_viewed_at: last_viewed_at
    }

    assert State.handle_call(:last_viewed_at, nil, state) == {:reply, last_viewed_at, state}
  end

  test "handle_cast/2" do
    {:ok, last_viewed_at, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")

    assert State.handle_cast({:last_viewed_at, last_viewed_at}, nil) ==
             {:noreply, %{last_viewed_at: last_viewed_at}}
  end

  test "exists?/1", %{preview: preview} do
    assert false == State.exists?(preview)
  end

  test "whereis/1", %{preview: preview} do
    assert nil == State.whereis(preview)
  end
end
