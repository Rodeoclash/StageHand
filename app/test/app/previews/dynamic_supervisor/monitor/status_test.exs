defmodule App.Previews.DynamicSupervisor.Monitor.StatusTest do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response,
    Previews,
    Previews.DynamicSupervisor.Monitor.Status
  }

  import App.Fixtures
  import Mox
  import Phoenix.ChannelTest

  use App.DataCase, async: false

  setup :verify_on_exit!

  setup do
    preview = insert(:preview)

    %{
      preview: preview
    }
  end

  test "name/1", %{preview: preview} do
    assert Status.name(preview) == {:monitor_status, "id9f12b94792201"}
  end

  test "via_name/1", %{preview: preview} do
    assert Status.via_name(preview) ==
             {:via, Registry, {:previews, {:monitor_status, "id9f12b94792201"}}}
  end

  test "start_link/1", %{preview: preview} do
    assert {:ok, pid} = Status.start_link([preview])
    assert is_pid(pid)
    GenServer.stop(pid)
    refute Process.alive?(pid)
  end

  test "init/1", %{preview: preview} do
    assert {:ok, state} = Status.init([preview])

    assert state == %{
             preview: preview
           }

    refute_received :work
    :timer.sleep(2000)
    assert_received :work
  end

  describe "handle_info/2" do
    test "with success", %{preview: preview} do
      # Needed to attach the logs module to
      Previews.DynamicSupervisor.start_child(preview)

      App.KubernetesV2Mock
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:ok,
           %{
             "items" => [
               Response.Pod.docker(preview),
               Response.Pod.preview(preview)
             ]
           }}
        )
      )
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:ok,
           %{
             "items" => [
               Response.Pod.docker(preview),
               Response.Pod.preview(preview)
             ]
           }}
        )
      )

      App.HTTPoisonMock
      |> expect(:request, fn method, request_url ->
        assert method == :get
        assert request_url == "http://id39da6a4e7d8ae.stagehand.home:32568"

        {:ok, %HTTPoison.Response{status_code: 200}}
      end)

      AppWeb.Endpoint.subscribe(AppWeb.PreviewChannel.channel_name(preview))
      state = %{preview: preview}

      assert {:noreply, state} == Status.handle_info(:work, state)
      :timer.sleep(2000)
      assert_received :work

      assert_broadcast("preview", %{
        data: %{
          provisioning: %{docker: true},
          readiness: [
            %{
              status: :ready,
              port: %{"name" => "App", "value" => 80}
            }
          ],
          status: %{code_extracted: true, code_fetched: true}
        }
      })

      Previews.DynamicSupervisor.terminate_child(preview)
    end

    test "with error", %{preview: preview} do
      App.KubernetesV2Mock
      |> expect(
        :run,
        Expectation.Pod.list(
          preview,
          {:error, %{}}
        )
      )

      state = %{preview: preview}

      assert {:noreply, state} == Status.handle_info(:work, state)
      :timer.sleep(2000)
      assert_received :work
    end
  end

  test "exists?/1", %{preview: preview} do
    assert false == Status.exists?(preview)
  end

  test "whereis/1", %{preview: preview} do
    assert nil == Status.whereis(preview)
  end
end
