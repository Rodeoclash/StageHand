defmodule App.Projects.ProviderAuthorisationsTest do
  alias App.Projects.ProviderAuthorisations
  import App.Fixtures
  use App.DataCase, async: true

  setup do
    provider = insert(:provider)
    project = insert(:project)
    user = insert(:user)
    provider_authorisation = insert(:provider_authorisation, user: user, provider: provider)
    insert(:provider_connection, project: project, provider: provider, user: user)

    %{
      project: project,
      provider: provider,
      provider_authorisation: provider_authorisation
    }
  end

  test "get/1", %{
    project: project,
    provider: provider,
    provider_authorisation: provider_authorisation
  } do
    fetched_provider_authorisation = ProviderAuthorisations.get(project, provider)
    assert fetched_provider_authorisation.id == provider_authorisation.id
  end
end
