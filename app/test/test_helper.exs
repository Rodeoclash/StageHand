Application.ensure_all_started(:hound)
ExUnit.start()

"test/support/mocks"
|> Path.join("**/*.exs")
|> Path.wildcard()
|> Enum.map(&Code.require_file/1)
