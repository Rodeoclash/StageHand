defmodule App.Fixtures do
  use ExMachina.Ecto, repo: App.Repo

  def user_factory do
    %App.Users.User{
      email: sequence(:installation_id, &"person#{&1}@example.com")
    }
  end

  def provider_factory do
    %App.Providers.Provider{
      name: "bitbucket"
    }
  end

  def provider_authorisation_factory do
    %App.ProviderAuthorisations.ProviderAuthorisation{
      expires: true,
      expires_at: 1_654_724_864,
      provider: build(:provider),
      token: "1",
      user: build(:user)
    }
  end

  def project_factory do
    seq =
      sequence(:installation_id, &Integer.to_string(&1))
      |> String.pad_leading(4, "0")

    %App.Projects.Project{
      id: "83b1101f-45ff-40fe-8618-3abcfd0452d6",
      access_token: "d61e6dbe-3237-43f9-a813-315f2e9d#{seq}",
      name: "HappyOrg"
    }
  end

  def role_factory do
    %App.Roles.Role{
      project: build(:project),
      user: build(:user)
    }
  end

  def provider_connection_factory do
    %App.ProviderConnections.ProviderConnection{
      project: build(:project),
      provider: build(:provider, %{name: "bitbucket"}),
      user: build(:user)
    }
  end

  def preview_factory do
    %App.Previews.Preview{
      id: "29e41d9a-4ca9-484b-82c1-df01dff3ab40",
      configuration: %{
        "command" => "sh",
        "description" => "An awesome preview",
        "env" => [
          %{
            "name" => "APP_ENV",
            "value" => "local"
          }
        ],
        "ports" => [
          %{
            "name" => "App",
            "value" => 80
          }
        ]
      },
      project: build(:project),
      provider: build(:provider),
      repo_slug: "repo_slug",
      sha: "sha",
      username: "username",
      version: 1
    }
  end

  def pod_docker_factory do
    %{
      "metadata" => %{
        "labels" => %{
          "app" => "docker"
        }
      },
      "status" => %{
        "containerStatuses" => [
          %{
            "name" => "docker",
            "restartCount" => 0,
            "ready" => true
          }
        ],
        "phase" => "Running"
      }
    }
  end

  def pod_preview_factory do
    %{
      "metadata" => %{
        "labels" => %{
          "app" => "preview"
        }
      },
      "status" => %{
        "phase" => "Pending"
      }
    }
  end
end
