defmodule App.Mocks.Http.Github do
  use ExUnit.Case, async: true

  def create_status(
        %{username: username, repo_slug: repo_slug, sha: sha, feedback: feedback},
        preview \\ nil
      ) do
    fn method, url, body, _headers ->
      assert method == :post

      assert url ==
               "https://api.github.com/repos/#{username}/#{repo_slug}/statuses/#{sha}"

      body = Poison.decode!(body)
      assert body["context"] == feedback["key"]
      assert body["description"] == feedback["name"]
      assert body["state"] == "success"

      if preview do
        assert body["target_url"] =~ "#{AppWeb.Endpoint.url()}/preview?id=#{preview.id}"
      else
        assert body["target_url"] =~ "#{AppWeb.Endpoint.url()}/preview?id="
      end

      {:ok, %HTTPoison.Response{}}
    end
  end
end
