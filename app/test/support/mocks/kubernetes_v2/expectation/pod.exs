defmodule App.Mocks.KubernetesV2.Expectation.Pod do
  alias App.{
    Mocks.KubernetesV2.Operation
  }

  use ExUnit.Case, async: true

  def list(preview, response) do
    fn operation ->
      assert operation == Operation.Pod.list(preview)
      response
    end
  end

  def list_docker(preview, response) do
    fn operation ->
      assert operation == Operation.Pod.list_docker(preview)
      response
    end
  end

  def list_preview(preview, response) do
    fn operation ->
      assert operation == Operation.Pod.list_preview(preview)
      response
    end
  end

  def log(preview, response) do
    fn operation, options ->
      assert options == [params: %{"container" => "preview"}]
      assert operation == Operation.Pod.log(preview)
      response
    end
  end
end
