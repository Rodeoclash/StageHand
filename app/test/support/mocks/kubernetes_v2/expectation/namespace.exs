defmodule App.Mocks.KubernetesV2.Expectation.Namespace do
  alias App.{
    Mocks.KubernetesV2.Operation
  }

  use ExUnit.Case, async: true

  def list(response) do
    fn operation, opts ->
      assert operation == Operation.Namespace.list()
      assert opts == [params: %{"labelSelector" => "stagehand=true"}]
      response
    end
  end

  def get(preview, response) do
    fn operation ->
      assert operation == Operation.Namespace.get(preview)
      response
    end
  end

  def patch(preview, response) do
    fn operation ->
      assert operation == Operation.Namespace.patch(preview)
      response
    end
  end

  def delete(preview, response) do
    fn operation ->
      assert operation == Operation.Namespace.delete(preview)
      response
    end
  end
end
