defmodule App.Mocks.KubernetesV2 do
  alias App.{
    Mocks.KubernetesV2.Expectation,
    Mocks.KubernetesV2.Response
  }

  import Mox

  use ExUnit.Case, async: true

  @doc """
  Configures all expectations for a full startup
  """
  def start(preview) do
    # monitor state startup last_viewed_at time
    App.UtilsMock
    |> expect(:utc_now, fn ->
      {:ok, result, _} = DateTime.from_iso8601("2019-07-01T11:03:58.970155Z")
      result
    end)

    App.KubernetesV2Mock
    |> expect(
      :async,
      Expectation.read_all(preview, Response.read_all_missing(preview))
    )
    |> expect(:async, Expectation.start(preview, Response.create_all(preview)))
  end

  def stop(preview) do
    App.KubernetesV2Mock
    |> expect(:run, Expectation.Namespace.delete(preview, Response.Namespace.delete(preview)))
  end
end
