defmodule AppWeb.AuthorisationErrorControllerTest do
  alias AppWeb.AuthorisationErrorController
  use AppWeb.ConnCase

  test "sends 401 error and message", %{conn: conn} do
    conn = AuthorisationErrorController.auth_error(conn, {"error_kind", "A reason for it"}, %{})
    assert response(conn, 401) =~ "error_kind"
  end
end
