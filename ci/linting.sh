#!/bin/sh

set -e

echo " === Linting ==="

docker-compose -f docker-compose.ci.yml \
  run \
  --no-deps \
  --rm \
  app \
  mix format --check-formatted
