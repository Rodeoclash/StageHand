#!/bin/sh

set -ex

echo " === Setup ==="

export IMAGE=registry.gitlab.com/rodeoclash/stagehand/app
export IMAGE_LATEST=${IMAGE}:latest
export IMAGE_VERSIONED=${IMAGE}:${CI_COMMIT_SHA}

# Login to registry
docker login -u Rodeoclash -p ${STAGEHAND_PERSONAL_ACCESS_TOKEN} registry.gitlab.com

# Dummy dev secrets config file so dev environment loads (allows loading dev env, e.g. for linting check)
touch $CI_PROJECT_DIR/app/config/dev.secret.exs
